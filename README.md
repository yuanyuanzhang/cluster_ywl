# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Pathfinding pipeline for cluster weak lensing modeling from cosmology directly.
* This is based on existing tSZ modeling code and Weak lensing modeling code. 

### How do I get set up? ###

Approach 1:
* Install [cosmosis](https://bitbucket.org/joezuntz/cosmosis/wiki/Home) using the manual approach and the associated [des library](https://bitbucket.org/joezuntz/cosmosis/wiki/des).
* Install Tom McClintock's mass modeling [toolkit](https://github.com/tmcclintock/cluster_toolkit). 
* Put this git repo in the cosmosis-des-library folder in the cosmosis folder, like: cosmosis/cosmosis-des-library/cluster_ywl
* Set up cosmosis, go to the cluster_ywl folder, and do: cosmosis y_cross_rm.ini

Approach 2:
Unfortunately, this package does not work with the comosis-bootstrap installation, and cosmosis manual installation is not straighforward. The manual installation requires a lot of dependencies. My hack is to install comosis twice, first through the cosmosis-bootstrap approach to acquire dependencies.

* Install [anaconda python 2.+ version](https://www.anaconda.com/download/#macos)
* Install [cosmosis](https://bitbucket.org/joezuntz/cosmosis/wiki/Home) using the cosmosis-bootstrap approach.
* Download the [cosmosis](https://bitbucket.org/joezuntz/cosmosis/wiki/Manual%20Install) manual package. You can skip the dependencies for now.
* Modify the setup-my-cosmosis files so that the gsl etc. are pointed to the cosmosis-bootstrap packages you just installed, and the python dependencies are pointed to the anaconda python you just installed.
* For the above step, the setup-my-cosmosis file in [this git repostiory](https://bitbucket.org/yuanyuanzhang/cluster_ywl.git) is an example that has worked on Fermilab DES computers.
* Build the [manul cosmosiss installation](https://bitbucket.org/joezuntz/cosmosis/wiki/Manual%20Install)
* At one point, you will have an error creating nicaea_interface.so. Go to the folder cosmosis-standard-library/shear/cl_to_xi_nicaea and create a placeholder file named nicaea_interface.so. Repeat the build step. Everything should work this time.
* After "build" is complete, install the des library using the [git approach](https://bitbucket.org/joezuntz/cosmosis/wiki/des). 
* Start cosmosis by doing "source setup-my-cosmosis". This should start the anaconda python that you just installed.
* Install [Tom's mass modeling toolkit](https://github.com/tmcclintock/cluster_toolkit)
* Install fitsio, numba etc (using pip install).
* git clone [this git repo](https://bitbucket.org/yuanyuanzhang/cluster_ywl.git). Put this git repo in the cosmosis-des-library folder in the cosmosis folder, like: cosmosis/cosmosis-des-library/cluster_ywl
* Set up cosmosis, go to the cluster_ywl folder, and do: cosmosis y_cross_rm.ini 
* At one point, you may get an error that says "libgsl.so.0 not found". To resolve it, go to the libgsl folder (the "GSL_LIB" folder in your set-up-mycosmosis file), and do "ln -s libgsl.so.0 libgsl.so"

Approach 3:
If you are logged into the Fermilab DES computers, it would be easier to use the exisitng cosmosis installation. You can also ask Yuanyuan for help with installing your own version on the Fermilab machines. 

The procedures of running the existing Fermilab installation is:

* Establish a proper folder somewhere, 
* git clone [this code](https://bitbucket.org/yuanyuanzhang/cluster_ywl.git)
* In y_cross_rm.ini, change the paths of wl_profile.py, mass_dist.py, tinker_bias.py, and ylike_temp.py to where you have the code.
* For example, if you have your version of the code stored in
/data/des60.a/data/maria/cluster_ywl,
change the follow lines so they become:

[mass_dist]
file = /data/des60.a/data/maria/cluster_ywl/mass_dist.py

[wl_profile]
file = /data/des60.a/data/maria/cluster_ywl/wl_profile.py

[ylike_temp]
file = /data/des60.a/data/maria/cluster_ywl/ylike_temp.py

[tinker_bias]
file = /data/des60.a/data/maria/cluster_ywl/tinker_bias.py

* Do: 
source /data/des40.a/data/ynzhang/cosmosis_manual/cosmosis-des-library/cluster_ywl/setup-my-cosmosis

* Do:
cosmosis y_cross_rm.ini


### Who do I talk to? ###

* Yuanyuan Zhang (ynzhang@fnal.gov)
* Vinu V., Jim A., Tom M.
