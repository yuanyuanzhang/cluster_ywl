import os
from cosmosis.datablock import option_section, names
import numpy as np
from scipy.interpolate import interp1d
from scipy.interpolate import RectBivariateSpline
#import sz_profile
#import scaling_relations
#import one_halo
#import two_halo
#from projection_convolution import projection_convolution
import scaling_relations
#import cluster_average as ca
import scipy.special

cosmo = names.cosmological_parameters
dis = names.distances
mf = names.mass_function
tbf = "tinker_bias_function"
mps = "matter_power_lin"
szb = "wl"
cosmosis_dir = "output_cross/"

def setup(options):
    """
    Idea:
         We have several sections and names which can be used to 
         store several cosmological parameters fixed during MCMC
         chains. I used the following for the sections
         1. section = mps ("matter_power_lin"): z, pk, k_h
         2. section = dis (names.distances): z, d_a, h
         3. section = mf (names.mass_function): z, m_h, dndlnMh
         4. section = tbf ("tinker_bias_function"): z, ln_mass, bias
         5. section = szb ("wl") 
    """
    section = option_section
    # control the calculation: szonly if skipping cosmology calculation
    szonly = int(options[section,"szonly"])
    saveOutput = int(options[section, "saveOutput"])
    # sample parameters
    psf_fwhm   = options[section,  "PSF"]
    fraction_of_sky = options[section, "fraction_of_sky"]
    z_bins = options[section, "z_bins"]
    #z_bins = options[section, "zbins_default"]
    mp_bins = options[section, "mp_bins"]
    skip_bins_i = options[section, "skip_bins_i"]
    skip_bins_j = options[section, "skip_bins_j"]
    # radial profile parameters
    r_min = options[section, "r_min"] #Angular diameter distance Mpc
    r_max = options[section, "r_max"]
    r_bins = int(options[section, "r_bins"])
    # power spectrum parameters
    m_bins = int(options[section, "m_bins"])
    k_bins = int(options[section, "k_bins"])
    # projection  parameters
    max_radius_ampc = options[section, "max_radius_ampc"]
    pixel_scale = options[section, "pixel_scale_arcmin"]

    return szonly, saveOutput, psf_fwhm, fraction_of_sky, z_bins, mp_bins, \
        r_min, r_max, r_bins, m_bins, k_bins, max_radius_ampc, pixel_scale, \
        skip_bins_i, skip_bins_j

def execute(block, config):
    """
Thermal SZ: Y cross RM cluster centers.
    """
    #lightspeed in km/s
    lightspeed = 3e5

    szonly, saveOutput, psf_fwhm, fraction_of_sky, z_bins, mp_bins, \
        r_min, r_max, r_bins, m_bins, k_bins, \
        max_radius_ampc, pixel_scale, skip_bins_i, \
        skip_bins_j = config

    #==================================
    #
    # sz profile parameters
    #
    #==================================
    Ap = block["sz_profile","Ap"]
    Bp = block["sz_profile","Bp"]
    Cp = block[cosmo,"Cp"]
    Ax = block[cosmo,"Ax"]
    Bx = block[cosmo,"Bx"]
    Cx = block[cosmo,"Cx"]
    Abeta = block[cosmo,"Abeta"]
    Bbeta = block[cosmo,"Bbeta"]
    Cbeta = block[cosmo,"Cbeta"]
    # our miscentering model parameters
    miscenter_tau      = block[cosmo,"miscenter_tau"]
    miscenter_fraction = block[cosmo,"miscenter_fraction"]
    # and we will use Mechior et al 2016 eq 51 and table 4
    m200_pivot =  block[cosmo,"m200_pivot"] * 1e14 # in units of 1e14 M_sun in .ini file
    Fm =  block[cosmo,"Fm"] # the lambda power law
    Cm =  block[cosmo,"Cm"] # the redshift power law
    m_sigma =  block[cosmo,"m_sigma"] # mass scatter
    lambda_pivot =  block[cosmo,"lambda_pivot"] # pivot of relation, if direct mass, = 1

    rarr = np.logspace(np.log10(r_min), np.log10(r_max), r_bins) #Ang. dia.

    #==================================
    #
    # cosmological parameters
    #
    #==================================
    #
    # cosmology
    #
    h0 = block[cosmo,"h0"]
    omega_b = block[cosmo,"omega_b"]
    omega_m = block[cosmo,"omega_m"]

    if szonly:
        dir = os.path.join(cosmosis_dir,"distances/")
        z_table = np.genfromtxt(os.path.join(dir,"z.txt"), skip_header=1)
        da_table = np.genfromtxt(os.path.join(dir,"d_a.txt"), skip_header=1)
        hz_table = np.genfromtxt(os.path.join(dir,"h.txt"), skip_header=1)*lightspeed
    else :
        z_table = block[dis,'z']
        da_table = block[dis,'d_a']
        hz_table = block[dis,'h']*lightspeed # 1/Mpc
    ix = np.argsort(z_table)
    hz_interp = interp1d(z_table[ix], hz_table[ix])
    da_interp = interp1d(z_table[ix], da_table[ix]/(60.*(360./2/np.pi))) # in Mpc/arcmin

    #
    # matter-matter power spectrum
    #
    if szonly:
        dir = os.path.join(cosmosis_dir,"matter_power_lin/")
        k_h = np.genfromtxt(os.path.join(dir,"k_h.txt"), skip_header=1)
        pk_z_table = np.genfromtxt(os.path.join(dir,"z.txt"), skip_header=1)
        p_k_table=np.genfromtxt(os.path.join(dir,"p_k.txt"), skip_header=1)
        p_k_table = p_k_table.reshape([np.size(pk_z_table),np.size(k_h)])
    else :
        k_h = block[mps, "k_h"]
        pk_z_table = block[mps, "z"]
        p_k_table=block[mps,"p_k"]
    spline = RectBivariateSpline(pk_z_table, k_h, p_k_table)
    k_h = np.logspace(np.log10(k_h.min()), np.log10(k_h.max()), k_bins)
    k = k_h*h0 # 1/Mpc

    #
    # tinker mass function  in critical
    #
    if szonly:
        dir = os.path.join(cosmosis_dir,"mass_function/")
        mz_table=np.genfromtxt(os.path.join(dir,"z.txt"), skip_header=1)
        mh_table=np.genfromtxt(os.path.join(dir,"m_h.txt"), skip_header=1)# h^2 Msol
        dndlnmh_table=np.genfromtxt(os.path.join(dir,"dndlnmh.txt"), skip_header=1)
        dndlnmh_table = dndlnmh_table.reshape([np.size(mz_table),np.size(mh_table)])
    else :
        mz_table=block[mf,"z"]
        mh_table=block[mf,"m_h"]
        dndlnmh_table=block[mf,"dndlnMh"]
    # Currently the nobs calcuated in one_halo is wrong by a factor of 4*pi*fraction_sky
    # we'll have to fix this once we merge  May 5 2017
    # area = 4.0*np.pi*fraction_of_sky  # fraction of the sky covered, taking mass function as per sterdadian
    # dndlnMh_table = dndlnMh_table*area
    mass_function = RectBivariateSpline(mz_table, np.log10(mh_table), dndlnmh_table)
    # decide what masses to evaluate at
    mh_table = np.logspace(7, 17, m_bins) #This is h^2 Msol
    ln_mh_table = np.log(mh_table)
    m_table = mh_table * h0 * h0 #This is Msol

    #
    # tinker bias
    #
    if szonly:
        dir = os.path.join(cosmosis_dir,"tinker_bias_function/")
        tinker_bias_ln_mass= np.genfromtxt(os.path.join(dir,"ln_mass_h.txt"),skip_header=1)
        tinker_bias_bias = np.genfromtxt(os.path.join(dir,"bias.txt"), skip_header=1)
        tinker_bias_zed = np.genfromtxt(os.path.join(dir,"z.txt"), skip_header=1)
    else :
        tinker_bias_ln_mass = block[tbf,"ln_mass_h"]
        tinker_bias_bias = block[tbf,"bias"]
        tinker_bias_zed = block[tbf,"z"]
    tinker_ln_mass_h = np.log(np.exp(tinker_bias_ln_mass) / h0 / h0 / h0) #h^2 Msol
    bias_interpolater_2d = RectBivariateSpline(\
         tinker_bias_zed, tinker_ln_mass_h, tinker_bias_bias)


    #==================================
    #
    # main computation
    #
    #=================================
    z_size = z_bins.size-1
    mp_size = mp_bins.size-1
    for i in range(z_size) :
        for j in range(mp_size) :
            ii = i==skip_bins_i
            ij = j == skip_bins_j[ii]
            if np.nonzero(ij)[0].size == 1: continue
            zlow = z_bins[i]
            zhi = z_bins[i+1]
            zed = (zlow + zhi) / 2.

            mp_low = mp_bins[j]
            mp_high = mp_bins[j+1]
            bin_name = "zbin_{}_mpbin_{}".format(str(i),str(j))

            #
            # cosmology
            #
            hz = hz_interp(zed)
            da = da_interp(zed)
                # JTA: disbelieve, this is true for what cosmologies?  A limited range, right?
                # We'll want to use cosmosis to calculate omega_mz
            omega_l = 1. - omega_m
            E0 =  (omega_l + omega_m * (1. + zed)**3)
            omega_mz = omega_m*(1.0 + zed)**3/E0 #omega_m(z)

            #
            # tinker mass function  in critical
            #
            dndlnmh_table = mass_function(zed, np.log10(mh_table)).flatten()
            dndlnm_table = dndlnmh_table*h0**3 #1/Mpc^3
            #Convert M200c masses to M200m
            rho_critical = scaling_relations.rho_critical(hz) #Msol/Mpc^3  
            #Generating some M200c masses
            tm200c = np.logspace(6, 18, 50)
            tm200m = np.array([scaling_relations.HuKravtsov(
                zed, mi, rho_critical, rho_critical, 200., 200.*omega_mz, h0, 0)[2] for mi in tm200c])
            #Interpolating M200m vs M200c
            tmspl = interp1d(tm200m, tm200c)
            mc_table = tmspl(m_table) #Msol
            #
            # tinker bias
            #
            tinker_bias = bias_interpolater_2d(zed, ln_mh_table).flatten() # for masses of m_table
            #
            # matter-matter power spectrum
            #
            p_k_table = spline(zed, k_h).flatten()
            p_k_table = p_k_table/h0**3  # 1/Mpc^3


            # Copy the mass to the true mass which is used for Eq. 17, 18, 19
            ln_mt200c_arr = np.log(mc_table)
            ln_mt200m_arr = np.log(mh_table*h0**2)
            pmz=np.zeros(len(ln_mt200c_arr))
            pmz=(dndlnm_table * lh4(ln_mt200m_arr, m_sigma, m200_pivot, Fm, Cm, zed, mp_low, mp_high, lambda_pivot))

            block[szb, 'pmz_{}'.format(bin_name)] = pmz
            block[szb, 'mh200m_{}'.format(bin_name)] = mh_table*h0**3
            block[szb, 'dndlnm_{}'.format(bin_name)] = dndlnm_table
            block[szb, 'ln_mt200c_arr_{}'.format(bin_name)] = ln_mt200c_arr

            if saveOutput:
                dir = os.path.join(cosmosis_dir,"tsz/")
                # one-halo
                np.savetxt(os.path.join(dir,"pmz_{}.txt".format(bin_name)),
                    pmz.T, "%e", header="# P(M|z, lambda_1, lambda_2)")
    return 0
# the Lima-Hu trick
'''def lh4(ln_mass_arr,  sigma_mass, m200_pivot, Fm, Cm, zed, mp_low, mp_high, lambda_pivot) :
    m200m = scaling_relations.mass_from_lambda (m200_pivot, Fm, Cm, zed, mp_low, lambda_pivot=lambda_pivot)
    ln_m200o_low =np.log( m200m )
    m200m = scaling_relations.mass_from_lambda (m200_pivot, Fm, Cm, zed, mp_high, lambda_pivot=lambda_pivot)
    ln_m200o_hi =np.log( m200m )
    
    pefc=np.zeros(len(ln_mass_arr))
    for ii in range(len(pefc)):
        x1 = (ln_m200o_low - ln_mass_arr[ii]) / np.sqrt(2.)/sigma_mass
        x2 = (ln_m200o_hi - ln_mass_arr[ii]) / np.sqrt(2.)/sigma_mass
        efc1 = scipy.special.erfc(x1)
        efc2 = scipy.special.erfc(x2)
        pefc[ii]= 0.5 * (efc1 - efc2)
    return pefc
'''
def lh4(ln_mass_arr,  sigma_lmd, m200_pivot, Fm, Cm, zed, mp_low, mp_high, lambda_pivot) :
    Bm=1.0/Fm
    Cz=-Cm/Fm
    ln_lambda_arr=scaling_relations.lnlambda_from_lnmass (Bm, Cz, zed, ln_mass_arr, m200_pivot, lambda_pivot)
    pefc=np.zeros(len(ln_mass_arr))
    for ii in range(len(pefc)):
        exp_lnlam=np.exp(ln_lambda_arr[ii])
        var_mp=sigma_lmd**2+np.maximum(0, (exp_lnlam-1)/exp_lnlam**2)
        x1 = (np.log(mp_low) - ln_lambda_arr[ii]) / np.sqrt(2.*var_mp)
        x2 = (np.log(mp_high) - ln_lambda_arr[ii]) / np.sqrt(2.*var_mp)
        efc1 = scipy.special.erfc(x1)
        efc2 = scipy.special.erfc(x2)
        pefc[ii]= 0.5 * (efc1 - efc2)
    return pefc



