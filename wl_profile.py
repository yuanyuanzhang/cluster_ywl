import os
from cosmosis.datablock import option_section, names
import numpy as np
from scipy.interpolate import interp1d
from scipy.interpolate import RectBivariateSpline
#import sz_profile
#import scaling_relations
#import one_halo
#import two_halo
#from projection_convolution import projection_convolution
#import scaling_relations
#import cluster_average as ca
import scipy.special
#import clusterwl
import cluster_toolkit as clusterwl
import numpy as np
from numba import jit

cosmo = names.cosmological_parameters
dis = names.distances
mf = names.mass_function
tbf = "tinker_bias_function"
mps = "matter_power_lin"
szb = "wl"
cosmosis_dir = "output_cross/"

def setup(options):
    section = option_section
    # control the calculation: szonly if skipping cosmology calculation
    saveOutput = int(options[section, "saveOutput"])
    # sample parameters
    z_bins = options[section, "z_bins"]
    mp_bins = options[section, "mp_bins"]
    skip_bins_i = options[section, "skip_bins_i"]
    skip_bins_j = options[section, "skip_bins_j"]

    return saveOutput, z_bins, mp_bins, \
           skip_bins_i, skip_bins_j

def execute(block, config):

    saveOutput, z_bins, mp_bins, \
        skip_bins_i, skip_bins_j = config
    #==================================
    #
    # main computation
    #
    #=================================
    omega_m = block[cosmo,"omega_m"]
    z_size = z_bins.size-1
    mp_size = mp_bins.size-1
    for i in range(z_size) :
        for j in range(mp_size) :
            ii = i==skip_bins_i
            ij = j == skip_bins_j[ii]

            if np.nonzero(ij)[0].size == 1: continue
            zlow = z_bins[i]
            zhi = z_bins[i+1]
            zed = (zlow + zhi) / 2.

            mp_low = mp_bins[j]
            mp_high = mp_bins[j+1]
            bin_name = "zbin_{}_mpbin_{}".format(str(i),str(j))
            pmz=block[szb, 'pmz_{}'.format(bin_name)]
            dndlnm_table=block[szb, 'dndlnm_{}'.format(bin_name)]
            ln_mt200c_arr=block[szb, 'ln_mt200c_arr_{}'.format(bin_name)]
            mh200m_arr=block[szb, 'mh200m_{}'.format(bin_name)]

            #---Maria add
            #dir_lin = os.path.join(cosmosis_dir,"matter_power_lin/")
            k = block["matter_power_lin", "k_h"]#np.genfromtxt(os.path.join(dir_lin,"k_h.txt"), skip_header=1)
            P_k = block["matter_power_lin", "p_k"]#np.genfromtxt(os.path.join(dir_lin,"p_k.txt"), skip_header=1)[3]

            #dir_nl = os.path.join(cosmosis_dir,"matter_power_nl/")
            k_nl = block["matter_power_nl", "k_h"]#np.genfromtxt(os.path.join(dir_nl,"k_h.txt"), skip_header=1)
            P_k_nl = block["matter_power_nl", "p_k"]#np.genfromtxt(os.path.join(dir_nl,"p_k.txt"), skip_header=1)[3]
            #---

            NR = 1000
            R = np.logspace(-2, 3, NR, base=10) #Xi_hm MUST be evaluated to higher than BAO
            wl_mt200m = []
            wl_mt200m_mm, wl_mt200m_2h, wl_mt200m_hm = [],[],[] #---Maria add----
            for sm in mh200m_arr:
                print sm # this is mass m200_mean
                c = 5
                # using functions from Tom M: https://github.com/tmcclintock/Cluster_WL/blob/master/examples/example.py
                Rp = np.logspace(-2, 2.4, NR, base=10)
                #--- Maria add
                xi_nfw   = clusterwl.xi.xi_nfw_at_R(R, sm, c, omega_m)
                xi_mm = clusterwl.xi.xi_mm_at_R(R, k_nl, P_k_nl) #Need the P_k non-linear!!!
                bias = clusterwl.bias.bias_at_M(sm, k, P_k, omega_m) #Need the P_k linear
                xi_2halo = clusterwl.xi.xi_2halo(bias, xi_mm)
                xi_hm = clusterwl.xi.xi_hm(xi_nfw, xi_2halo) #xi_halo_matter


                Sigma_mm  = clusterwl.deltasigma.Sigma_at_R(Rp, R, xi_mm, sm, c, omega_m)
                DeltaSigma_mm = clusterwl.deltasigma.DeltaSigma_at_R(Rp, Rp, Sigma_mm, sm, c, omega_m)
                Sigma_2h  = clusterwl.deltasigma.Sigma_at_R(Rp, R, xi_2halo, sm, c, omega_m)
                DeltaSigma_2h = clusterwl.deltasigma.DeltaSigma_at_R(Rp, Rp, Sigma_2h, sm, c, omega_m)
                Sigma_hm  = clusterwl.deltasigma.Sigma_at_R(Rp, R, xi_hm, sm, c, omega_m)
                DeltaSigma_hm = clusterwl.deltasigma.DeltaSigma_at_R(Rp, Rp, Sigma_hm, sm, c, omega_m)

                
                Sigma_nfw = clusterwl.deltasigma.Sigma_nfw_at_R(Rp, sm, c, omega_m)
                DeltaSigma_nfw = clusterwl.deltasigma.DeltaSigma_at_R(Rp, R, Sigma_nfw, sm, c, omega_m)
                #xi_nfw   = clusterwl.xi.xi_nfw_at_R(R, sm, c, omega_m)
                wl_mt200m.append( Sigma_nfw )
                wl_mt200m_mm.append( Sigma_mm )
                wl_mt200m_2h.append( Sigma_2h )
                wl_mt200m_hm.append( Sigma_hm )
                #---
            wl_mt200m = np.array(wl_mt200m)
            #---Maria add
            wl_mt200m_mm = np.array(wl_mt200m_mm)
            wl_mt200m_2h = np.array(wl_mt200m_2h)
            wl_mt200m_hm = np.array(wl_mt200m_hm)
            #---

            #xi_temp=clusterwl.xi.xi_nfw_at_R(R, 3.0*10.0**14.0, c, om) # for comparison only
            total_h1_corr=value_xi(R, wl_mt200m, ln_mt200c_arr, pmz)
            nobs=value_nobs(ln_mt200c_arr, pmz)
            h1_corr = total_h1_corr / nobs
            block[szb, 'DeltaSigma_{}'.format(bin_name)]=h1_corr
            block[szb, 'DeltaSigma_r_{}'.format(bin_name)]=Rp
            #--- Maria add
            total_mm_corr=value_xi(R, wl_mt200m_mm, ln_mt200c_arr, pmz)
            mm_corr = total_mm_corr / nobs
            block[szb, 'DeltaSigmamm_{}'.format(bin_name)]=mm_corr

            total_h2_corr=value_xi(R, wl_mt200m_2h, ln_mt200c_arr, pmz)
            h2_corr = total_h2_corr / nobs
            block[szb, 'DeltaSigma2h_{}'.format(bin_name)]=h2_corr

            total_hm_corr=value_xi(R, wl_mt200m_hm, ln_mt200c_arr, pmz)
            hm_corr = total_hm_corr / nobs
            block[szb, 'DeltaSigmahm_{}'.format(bin_name)]=hm_corr
            #---
            if saveOutput:
                dir = os.path.join(cosmosis_dir,"wl/")
                # one-halo
                np.savetxt(os.path.join(dir,"pmz_h1corr_{}.txt".format(bin_name)),
                    h1_corr.T, "%e", header="# 1halo delta_sigma profiles")
                np.savetxt(os.path.join(dir,"pmz_h1corr_r_{}.txt".format(bin_name)),
                    R.T, "%e", header="# 1halo delta_sigma profiles")
                #np.savetxt(os.path.join(dir,"pmz_h1corr_comp_{}.txt".format(bin_name)), xi_temp.T, "%e", header="# 1halo mass profiles")
                #--- Maria add
                # matter-matter, two-halo, halo-matter
                np.savetxt(os.path.join(dir,"pmz_mmcorr_{}.txt".format(bin_name)),
                    mm_corr.T, "%e", header="# mm delta_sigma profiles")
                np.savetxt(os.path.join(dir,"pmz_h2corr_{}.txt".format(bin_name)),
                    h2_corr.T, "%e", header="# 2halo delta_sigma profiles")
                np.savetxt(os.path.join(dir,"pmz_hmcorr_{}.txt".format(bin_name)),
                    hm_corr.T, "%e", header="# hm delta_sigma profiles")

    return 0

@jit(nopython=False)
def value_nobs(ln_mt200c_arr, pmz):
    '''
    Eq. 18
    Integrating dn/dlnM * (erfc(xL) - erfc(xH))/2
    '''
    dlnm = ln_mt200c_arr[1] - ln_mt200c_arr[0]
    nobs = 0.
    for i in range(len(ln_mt200c_arr)):
        nobs += pmz[i]
    nobs *= dlnm
    return nobs

@jit(nopython=False)
def value_xi(rarr, wl_mt200m, ln_mt200c_arr, pmz):
    '''
    Eq. 19
    Integrating xi(M) * dn/dlnM * (erfc(xL) - erfc(xH))/2
    '''
    xi_lh = np.zeros(len(rarr))
    dlnm = ln_mt200c_arr[1] - ln_mt200c_arr[0]
    for i in range(len(rarr)):
        txi = wl_mt200m[:,i]
        xir = 0.
        for j in range(len(ln_mt200c_arr)):
            xir += (txi[j] * pmz[j] )
        xi_lh[i] = (xir * dlnm)
    return xi_lh
