import numpy as np
import scaling_relations
from scipy.interpolate import interp1d

def lambda_from_mass (zed, m200, Ho, ol, om, ok) :
    Alambda = 66.1
    Blambda = 1.14
    Clambda = 0.73
    lam = scaling_relations.lambda_from_mass(
            Alambda, Blambda, Clambda, zed, m200, Ho, ol, om, ok)
    return lam

def mass_from_lambda (zed, rm_lambda, Ho=70., ol=0.70, om=0.30, ok=0.0, ml_interp="") :
    if ml_interp == "" :
        ml_interp = mass_from_lambda_interp (zed, Ho, ol, om, ok) 

    mass = ml_interp(rm_lambda)
    return mass, ml_interp

def mass_from_lambda_interp (zed, Ho, ol, om, ok) :
    mrange = np.logspace(12.5,15.5,100)
    mrange = mrange/1e14
    lrange = []
    for m in mrange:
        lrange.append( lambda_from_mass(zed, m, Ho,ol, om, ok) )

    ml_interp = interp1d(lrange, mrange)
    return ml_interp



