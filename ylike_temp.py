from cosmosis.datablock import option_section, names
import numpy as np
from scipy.interpolate import interp1d
import os.path
#import astropy.io.fits as pyfits
import fitsio

cosmo = names.cosmological_parameters
szb = "tSZ"
cosmosis_dir = "output_cross/"

def setup(options):
    section = option_section
    print section
    # sample parameters
    z_bins = options[section, "z_bins"]
    mp_bins = options[section, "mp_bins"]
    skip_bins_i = options[section, "skip_bins_i"]
    skip_bins_j = options[section, "skip_bins_j"]
    # where the data is
    section = "ylike"

    return z_bins, mp_bins, skip_bins_i, skip_bins_j


def execute(block, config):
    test = False
    likes = names.likelihoods
    z_bins, mp_bins, skip_bins_i, skip_bins_j = config

    y_like_total = 0
    z_size = z_bins.size-1
    mp_size = mp_bins.size-1
    for i in range(z_size) :
        for j in range(mp_size) :
            ii = i==skip_bins_i
            ij = j == skip_bins_j[ii]
            if np.nonzero(ij)[0].size == 1: continue
            zlow = z_bins[i]
            zhi = z_bins[i+1]
            mp_low = mp_bins[j]
            mp_high = mp_bins[j+1]
            bin_name = "zbin_{}_mpbin_{}".format(str(i),str(j))
            y = block[szb,'pmz_{}'.format(bin_name)] 
    

            y_like = -0.5*y.sum()
            y_like_total += y_like
    block[likes, 'YLIKE_LIKE'] = y_like_total
    return 0
    
def cleanup(config):
    #nothing to do here!  We just include this 
    # for completeness
    return 0
