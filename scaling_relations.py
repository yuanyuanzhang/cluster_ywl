import numpy as np
from numba import jit

#
# The support routines
#

def scaling_relation (A, B, C, zed, m200) :
    scale = A * m200**B * ((1+zed)/1.0)**C
    return scale
def pressure_from_mass (Ap, Bp, Cp, zed, m200) :
    pressure = scaling_relation(Ap, Bp, Cp, zed, m200)
    return pressure
def xc_from_mass (Ax, Bx, Cx, zed, m200) :
    xc = scaling_relation(Ax, Bx, Cx, zed, m200)
    return xc
def beta_from_mass (Abeta, Bbeta, Cbeta, zed, m200) :
    beta = scaling_relation(Abeta, Bbeta, Cbeta, zed, m200)
    return beta

def lambda_from_mass (Bm, Cz, zed, m200, mass_pivot, lambda_pivot) :
    lambdas= lambda_pivot * (m200/float(mass_pivot))**Bm * ( (1+zed)/1.5)**Cz
    return lambdas
def lnlambda_from_lnmass (Bm, Cz, zed, ln_m200, mass_pivot, lambda_pivot) :
    ln_lambdas= np.log(lambda_pivot) +Bm*(ln_m200-np.log(float(mass_pivot))) +Cz*np.log( (1+zed)/1.5)
    return ln_lambdas


@jit(nopython=True)
def mass_from_lambda (mass_pivot, fl, gz, zed, lambdas, lambda_pivot=30.) :
    '''
    Mechior et al 2016 eq 51 and table 4
    Melchior works in M200m
    mass_pivot = 2.35e14
    fl = 1.12
    gz = 0.18
    Input:
        mass_pivot: In linear scale
    Output:
        Mass in linear
    '''
    mass = mass_pivot * (lambdas/float(lambda_pivot))**fl * ( (1+zed)/1.5)**gz
    return mass


@jit(nopython=True)
def r200_from_mass (hz,m200) :
    '''
    m200 is in units of solar masses
    hz in the unit of km/s/Mpc
    G needs to be in units of solar mass
    G = 6.67259e-8  ;# cm^3 gm^-1 s^-2
    G = G * (1e-5)**3 ;# (km/cm)^3
    G = G * 1.99e33  ;# gm/M_sun to reach G in units of km^3/s^2/M_sun = 1.328e11
    '''
    G = 1.3278e+11 #gm/M_sun
    
    # r200 = (G/100) * m200^1/3 / H(z)^2/3  (equation 13)
    # h will be in km/s/Mpc
    # if you work through the units, there is one km/Mpc conversion left
    # going from G M200/H^2  3.086e+19 ;# km/Mpc
    r200 = ((G/100.) * m200 * hz**(-2.) * (3.086e19)**-1 )**(1./3.) 
    return  r200

@jit(nopython=True)
def f_from_Y(Y=0.24) :
    '''
    Y is the helium fraction. 
    See Vikram, Lidz, Jain, the dicussion after eq 6
    '''
    return (4.-2*Y)/(8.-5*Y)

     
def p200_from_mass(hz,m200,omega_b,omega_m) :
    '''
    Equation 28b in my Notes
    pressure has units of kg /m/s^2  
    '''
    # m200 is in units of 10^14 solar masses
    # hz in km/s/Mpc

    # G in km^3/s^2/solar mass: G = 1.328e11
    # (M^2/G)**(1./3.) = 1.7833e11  m_sun^2/km^2/s^2
    # converting H(z) from km/s/Mpc to 1/s: 3.086e19 km/Mpc
    # km_per_mpc = 3.086e19
    # a = (3./8./np.pi) * (1./2.)**(1./3)*200**(4/3.) # = 25.264
    b = omega_b/omega_m                  # = 0.164
    c = hz**(8./3.)                      # = 9.30e4 for Ho=72
    # (M^2/G)**(1./3.) = 4.222e5   M_sun s^2/km
    # d =  4.222e5 * km_per_mpc    # 1.303e25  M_sun s^2/mpc
    # e = km_per_mpc**(-8./3.)  # convert km/s/mpc to 1/s
    #p200 = 0.458 * m200**(2./3.) * hz**(8./3.) * omega_b/omega_m
    #a * d * e / 2 = 7.7e-26
    p200 = m200**(2./3.)* b * c * 7.7e-26 ;# 4.07e-22/2.
    return p200

def p200_from_mass2(hz, m200, omega_b, omega_m) :
    '''
    Eq 6 in Vikram, Lidz, Jain
    used to double check the above. It works.
    '''
    r200 = r200_from_mass(hz, m200)
    rho_c = rho_crit(hz)
    #print "r200", r200/1.1
    #raise Exception("here")
    # m200 is in units of 10^14 solar masses
    # hz in km/s/Mpc
    # G needs to be in units of solar mass
    # G = 6.67259e-8  ;# cm^3 gm^-1 s^-2
    # G = G * (1e-5)**3 ;# (km/cm)^3
    # G = G * 1.99e33  ;# gm/M_sun   to reach G in units of km^3/s^2/M_sun = 1.328e11 
    # G = G * (1./3.086e19)**3 ;# (Mpc/km)**3    to reach G in units of Mpc^3/s^2/M_sun = 4.519e-48
    G = 4.519e-48 #Mpc^3/s^2/M_sun 
    m200 = m200*1e14  ;# to get into M_sun units
    # M_sun km^2 /Mpc^3/s^2
    p200 = 200 * rho_c * (omega_b/omega_m ) * G * m200/(2.*r200)
    # Mpc^3/s^2/M_sun * M_sun/Mpc^3 M_sun Mpc^-1
    # s^-2  M_sun Mpc-1 = M_sun/Mpc/s^2
    #print "compare p200, p200_2,", p200, p200_from_mass2(hz,m200/1e14,omega_b,omega_m)
    return p200

@jit(nopython=True)
def rho_critical(hz, G=1.328e11) :
    '''
    support for the above test routine, otherwise not used
    '''
    # hz in km/s/Mpc
    # G in units of km^3/s^2/M_sun, need one more conversion
    G = G * (1./3.086e19) # G in units of km^2 Mpc/s^2/M_sun
    rho = 3.*hz**2 /8./ np.pi/ G
    # rho in units of M_sun/Mpc^3
    return rho

@jit(nopython=True)
def concentration_duffy(Mvir, z, cosmo_h):
    '''Duffy 2008'''
    conc = (7.85 / (1. + z)**0.71) * (Mvir * cosmo_h / 2e12)**-0.081
    return conc

@jit(nopython=True)
def concentration_duffy_200(M200, z, cosmo_h):
    '''Duffy 2008'''
    conc200 = (5.71 / (1. + z)**0.47) * (M200 * cosmo_h / 2e12)**-0.084
    return conc200

@jit(nopython=True)
def HuKravtsov(z, M, rho, rhoo, delta, deltao, cosmo_h, mvir):
    '''
    Eq. C10 in Hu&Kravstov to convert virial mass to any mass within some delta
    either both in critical density or mean density. In this function I use
    critical density. deltao is appropriately multiplied by omega(z) to find 
    output mass. i.e. lets say the virial critical mass is mass within a radius 
    which contains the average density is Delta_v*rho_critical and to find the
    mass within the average mean density of 200 mean density, then I use
    200 * (Omega(z) * rho_critical) = 50 * rho_critical. i.e. mass within
    50 times rho_critical 
    Input: 
           z : redshift
           M which either Mvir or M200c solar mass
           rho : Rho used for Mvir or M200c
           rhoo : DOESN'T involves in any calculation of the function 
                  (output density)
           delta : fraction of rho corresponds to Mvir or M200c
           deltao : fraction of rho corresponds to output mass 
           mvir : 0/1 
    '''
    a1 = 0.5116
    a2 = -0.4283
    a3 = -3.13e-3
    a4 = -3.52e-5
    Delta = deltao / delta
    if mvir:
        conc = concentration_duffy(M, z, cosmo_h)
        B = -0.081
    else:
        #raise ValueError('mvir should be 1')
        conc = concentration_duffy_200(M, z, cosmo_h)
        B = -0.084
    R = (M / ((4 * np.pi / 3.) * delta * rho))**(1/3.) #(Msun / Msun Mpc^(-3))1/3. -> Mpc    
    rho_s = rho * (delta / 3.) * conc**3. / (np.log(1 + conc) - conc / (1 + conc)) #Msun / Mpc^3  
    Rs = R / conc

    A = np.log(1.+conc) - 1. + 1. / (1. + conc)
    f = Delta * (1./conc**3) * A
    p = a2 + a3 * np.log(f) + a4 * np.log(f)**2
    x = (a1 * f**(2.*p) + 0.75**2.)**(-0.5) + 2. * f
    Mfrac = M * Delta * (1./conc/x)**3
    Rfrac = (3. * Mfrac / deltao / rho / 4. / np.pi)**(1./3.)
    return M, R, Mfrac, Rfrac, rho_s, Rs


@jit(nopython=True)
def dlnMdensitydlnMcritOR200(delta, delta1, M, M1, z, cosmo_h, mvir):
    '''
    Make sure that delta and delta1 is above critical density. 

    M is corresponds to delta definition 

    delta1 - standard mass definition. i.e. the mass corresponds 
    to either virial or 200 times critical density

    M1 is corresponds to delta1 definition 
    '''
    #print delta, delta1, M, M1,z
    a1 = 0.5116
    a2 = -0.4283
    a3 = -3.13e-3
    a4 = -3.52e-5
    Delta = delta / delta1
    if mvir:
        conc = concentration_duffy_200(M1, z, cosmo_h)
    else:
        conc = concentration_duffy(M1, z, cosmo_h)
    A = np.log(1.+conc) - 1. + 1. / (1. + conc)
    f = Delta * (1./conc**3) * A
    p = a2 + a3 * np.log(f) + a4 * np.log(f)**2
    x = (a1 * f**(2.*p) + 0.75**2.)**(-0.5) + 2. * f
    B = -0.081
    t31 = Delta / conc**2 * (1./(1. + conc) - 1./(1. + conc)**2.) - 3. * Delta * A / conc**3.
    t32 = 2. - p * a1 * f**(2*p-1.) * (a1 * f**(2*p) + 0.75**2.)**-1.5
    dMdM1 = Delta / (conc * x)**3 * (1. - 3 * B / x * (x + t31 * t32))
    #print dMdM1
    #print conc, f, p, x, t1, t2
    ##t21 = 1./conc**3 * (2./(1. + conc) - conc / (1. + conc)**2.)
    ##t22 = (3. / conc**4) * (np.log(1. + conc) + conc / (1. + conc))
    ##t2 = t2 * (t21 - t22)
    ##t21 = -3 * f / conc + f / A * (1./(1.+conc) - 1./(1.+conc)**2.)
    #print t2, t21, t22
    dlnMdlnM1 = (M1/M) * dMdM1
    return dlnMdlnM1

if __name__=='__main__':

    mass_pivot = 10**14.371
    zed = 0.4
    fl = 1.12
    gz = 0.18
    lambdas = 20.
    print '%.2e'%mass_from_lambda(mass_pivot, fl, gz, zed, lambdas)
    print '%.2f'%r200_from_mass (70,1e16)
