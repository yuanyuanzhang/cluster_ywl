import numpy as np
import matplotlib.pyplot as plt
import os
import glob

def calc_lmd(m):
    m0=10.0**14.371
    zfac=(1.50/1.50)**0.18
    lmds=(m/m0/zfac)**(1.0/1.12)*30
    return lmds

files=glob.glob('./*008.txt')

halos_all=np.empty([0, 6])
for halofile in files:
    halos=np.genfromtxt(halofile)
    halos_all=np.vstack([halos_all, halos])

ind, =np.where((halos_all[:, 4] > 2.0*10.0**14.0) & (halos_all[:, 4] < 2.1*10.0**14.0))
#ind, =np.where((halos_all[:, 5] > 20) & (halos_all[:, 5] < 22))
print np.std(np.log(halos_all[ind, 5]))
print np.mean(np.log(halos_all[ind, 5]))
print np.var(np.log(halos_all[ind, 5]))
print 0.25**2+(calc_lmd(np.mean(halos_all[ind, 4])/0.7)-1.0)/(calc_lmd(np.mean(halos_all[ind, 4])/0.7))**2
print calc_lmd(np.mean(halos_all[ind, 4])/0.7)
print calc_lmd(2.0*10.0**14.0/0.7)

ind, =np.where((halos_all[:, 4] > 3.0*10.0**14.0) & (halos_all[:, 4] < 3.05*10.0**14.0))
#ind, =np.where((halos_all[:, 5] > 30) & (halos_all[:, 5] < 32))
print np.std(np.log(halos_all[ind, 5]))
print np.mean(np.log(halos_all[ind, 5]))
print np.mean(np.log10(halos_all[ind, 4]))
print np.var(np.log(halos_all[ind, 5]))
print 0.25**2+(calc_lmd(np.mean(halos_all[ind, 4])/0.7)-1.0)/(calc_lmd(np.mean(halos_all[ind, 4])/0.7))**2
print calc_lmd(np.mean(halos_all[ind, 4])/0.7)
print calc_lmd(3.0*10.0**14.0/0.7)

mlmds = np.logspace(12, 16, 500, base=10)
lmds=calc_lmd(mlmds)
'''
plt.loglog(halos_all[:, 4]/0.6704, halos_all[:, 5], 'k.');
plt.plot(mlmds, lmds, 'b--')
plt.show()
'''

dndlnm=np.genfromtxt('../wl/dndlnm_zbin_0_mpbin_0.txt')
mh=np.genfromtxt('../wl/mh200m_zbin_0_mpbin_0.txt')
ind, =np.where(halos_all[:, 4] > 3.0*10.0**14.0)
m200bh=halos_all[ind, 4]
n, bins, patches=plt.hist(np.log(m200bh), bins=20, normed=True)
ind=np.where(np.log(mh) > bins[0])
mh=mh[ind]
dndlnm=dndlnm[ind]
plt.plot(np.log(mh), dndlnm*np.max(n)/np.max(dndlnm), 'k--')
plt.show()

