import numpy as np
import scipy.integrate
import scipy.special
from numba import jit

@jit(nopython=False)
def value_nobs(ln_mt200c_arr, dndlnm_table, ln_m200o_low, ln_m200o_hi, m_sigma):
    '''
    Eq. 18 
    Integrating dn/dlnM * (erfc(xL) - erfc(xH))/2
    '''
    dlnm = ln_mt200c_arr[1] - ln_mt200c_arr[0]
    nobs = 0.
    for i in range(len(ln_mt200c_arr)):
        nobs += (dndlnm_table[i] * lh4(ln_mt200c_arr[i],  ln_m200o_low, ln_m200o_hi, m_sigma))
        #print ln_mt200c_arr[i],  ln_m200o_low, ln_m200o_hi, m_sigma, lh4(ln_mt200c_arr[i],  ln_m200o_low, ln_m200o_hi, m_sigma)
    nobs *= dlnm
    return nobs     

@jit(nopython=False)
def value_xi(press_mt200c, ln_mt200c_arr, rarr, dndlnm_table, 
             ln_m200o_low, ln_m200o_hi, m_sigma):
    '''
    Eq. 19 
    Integrating xi(M) * dn/dlnM * (erfc(xL) - erfc(xH))/2
    '''
    xi_lh = np.zeros(len(rarr))
    dlnm = ln_mt200c_arr[1] - ln_mt200c_arr[0]
    for i in range(len(rarr)):
        txi = press_mt200c[:,i]
        xir = 0.
        for j in range(len(ln_mt200c_arr)):
            xir += (txi[j] * dndlnm_table[j] * lh4(ln_mt200c_arr[j],  ln_m200o_low, ln_m200o_hi, m_sigma))
        xi_lh[i] = (xir * dlnm)
    return xi_lh

@jit(nopython=False)
def value_bias(tinker_bias, ln_mt200c_arr, dndlnm_table, ln_m200o_low, ln_m200o_hi, m_sigma):
    '''
    Eq. 18 
    Integrating dn/dlnM * (erfc(xL) - erfc(xH))/2
    '''
    dlnm = ln_mt200c_arr[1] - ln_mt200c_arr[0]
    bias = 0.
    for i in range(len(ln_mt200c_arr)):
        bias += (tinker_bias[i] * dndlnm_table[i] * lh4(ln_mt200c_arr[i],  ln_m200o_low, ln_m200o_hi, m_sigma))
    bias *= dlnm
    return bias

# the Lima-Hu trick
def lh4(ln_mass,  ln_m200o_low, ln_m200o_hi, sigma_mass) :
    x1 = (ln_m200o_low - ln_mass) / np.sqrt(2.)/sigma_mass
    x2 = (ln_m200o_hi - ln_mass) / np.sqrt(2.)/sigma_mass
    efc1 = scipy.special.erfc(x1)
    efc2 = scipy.special.erfc(x2)
    return 0.5 * (efc1 - efc2)

